import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/shared/home/Home'
import Detail from '@/components/shared/detail/Detail'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/detail/:id',
      name: 'Detalhe',
      component: Detail  
    }
  ],
  mode: 'history'
})
