export default {
    medicos: [
        { Id: '1', nome: 'Fernando Mario Sefrin', endereco: 'Félix da Cunha, 183', telefone: '(53) 3240-3200', especialidade: 'Anestesiologia', imagem: '' },
        { Id: '2', nome: 'Marcelo', endereco: 'General Osório, 1184', telefone: '(53) 3240-3200', especialidade: 'Cirurgiao', imagem: '' },
        { Id: '3', nome: 'Sidney S. Paiva', endereco: 'Alcebíades Gotan, 1277', telefone: '(53) 3240-3200', especialidade: 'Anestesiologia', imagem: '' },
        { Id: '4', nome: 'Daiane Sonza Vidart', endereco: 'Tupy Silveira, 1077', telefone: '(53) 3240-3200', especialidade: 'Dermatologia', imagem: '' },
        { Id: '5', nome: 'Cristiane Casseres Teixeira', endereco: 'João Teles, 1277', telefone: '(53) 3240-3200', especialidade: 'Dermatologia', imagem: '' },
        { Id: '6', nome: 'Ricardo Moraes Lucas', endereco: '7 de setembro, 130', telefone: '(53) 3240-3200', especialidade: 'Cirurgia geral', imagem: '' },
        { Id: '7', nome: 'Jacob Ott', endereco: 'Marechal Deodoro, 1388', telefone: '(53) 3240-3200', especialidade: 'Cirurgia geral', imagem: '' },
        { Id: '8', nome: 'Renato de Souza Salim', endereco: 'Marechal Deodoro, 1300', telefone: '(53) 3240-3200', especialidade: 'Cardiologia', imagem: '' },
        { Id: '9', nome: 'Sergio Fernando da Silva Fraga', endereco: 'Félix Gotan, 1277', telefone: '(53) 3240-3200', especialidade: 'Cardiologia', imagem: '' }
    ]
}
